use rcocos2d_sys::cocos2d_CCNode as CCNode;
use rcocos2d_sys::cocos2d_CCPoint as CCPoint;
use rcocos2d_sys::cocos2d_CCSize as CCSize;
use rcocos2d_sys::cocos2d_CCSprite as CCSprite;

type Void = ();

pub fn create_layer_bg() -> Option<CCSprite> { None }
pub fn add_list_borders(_to: CCNode, _center: CCPoint, _size: CCSize) -> Void {}
