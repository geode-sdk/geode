use {
    libc::c_void,
    rcocos2d_sys::DWORD,
    std::{
        io,
        os::windows::{ffi::OsStrExt, fs::MetadataExt},
        path::{Path, PathBuf},
        ptr,
        sync::mpsc::{channel, Receiver},
        thread,
    },
    winapi::{
        shared::{minwindef::DWORD, winerror::WAIT_TIMEOUT},
        um::{
            fileapi::{CreateFileW, FindFirstChangeNotificationW, OPEN_EXISTING},
            handleapi::{CloseHandle, INVALID_HANDLE_VALUE},
            ioapiset::GetOverlappedResult,
            minwinbase::{OVERLAPPED_u, OVERLAPPED_u_s, OVERLAPPED},
            synchapi::CreateEventW,
            winbase::{
                ReadDirectoryChangesW, FILE_FLAG_BACKUP_SEMANTICS, FILE_FLAG_OVERLAPPED,
                WAIT_OBJECT_0,
            },
            winnt::{
                FILE_ACTION_MODIFIED, FILE_LIST_DIRECTORY, FILE_NOTIFY_CHANGE_ATTRIBUTES,
                FILE_NOTIFY_CHANGE_LAST_WRITE, FILE_NOTIFY_CHANGE_SIZE, FILE_NOTIFY_INFORMATION,
                FILE_SHARE_DELETE, FILE_SHARE_READ, FILE_SHARE_WRITE, HANDLE,
            },
        },
    },
};

type FileWatchCallback = Box<dyn FnMut(PathBuf) + Send>;
type ErrorCallback = Box<dyn FnMut(&str) + Send>;

struct FileWatcher {
    file: PathBuf,
    is_file: bool,
    callback: Option<FileWatchCallback>,
    error_callback: Option<ErrorCallback>,
    handle: HANDLE,
    thread_handle: Option<thread::JoinHandle<()>>,
}

impl FileWatcher {
    fn new<P>(
        file: P,
        callback: FileWatchCallback,
        error_callback: ErrorCallback,
    ) -> io::Result<FileWatcher>
    where
        P: AsRef<Path>,
    {
        let file = file.as_ref().to_path_buf();
        let is_file = file.is_file();
        let handle = unsafe {
            FindFirstChangeNotificationW(
                (match is_file {
                    true => file.parent().unwrap_or_else(|| Path::new(".")),
                    false => &file,
                })
                .as_os_str()
                .encode_wide()
                .chain(Some(0)),
                0,
                FILE_NOTIFY_CHANGE_LAST_WRITE
                    | FILE_NOTIFY_CHANGE_ATTRIBUTES
                    | FILE_NOTIFY_CHANGE_SIZE,
            )
        };

        if handle == INVALID_HANDLE_VALUE {
            return Err(io::Error::last_os_error());
        }

        let (tx, rx) = channel();
        let watcher = FileWatcher {
            file: file.clone(),
            is_file,
            callback: Some(callback),
            error_callback: Some(error_callback),
            handle,
            thread_handle: Some(start_watch_thread(file, rx)),
        };

        Ok(watcher)
    }
}

impl Drop for FileWatcher {
    fn drop(&mut self) {
        unsafe {
            CloseHandle(self.handle);
        }
    }
}

fn start_watch_thread<P>(file: P, receiver: Receiver<()>) -> thread::JoinHandle<()>
where
    P: std::marker::Send,
    P: AsRef<Path> + 'static,
{
    thread::spawn(move || {
        let file = file.as_ref().to_path_buf();
        let mut buffer = vec![0u8; 1024];

        'outer: loop {
            match receiver.recv() {
                Ok(()) => break,
                Err(_) => {
                    if watch_file(&file, &mut buffer).is_err() {
                        break 'outer;
                    }
                }
            }
        }
    })
}

fn as_mut_c_void<T>(value: T) -> *mut winapi::ctypes::c_void {
    let ptr: *mut winapi::ctypes::c_void =
        Box::into_raw(Box::new(value)) as *mut winapi::ctypes::c_void;

    ptr
}

fn watch_file<P>(file: P, buffer: &mut [u8]) -> io::Result<()>
where
    P: AsRef<Path>,
{
    let file = file.as_ref();
    let is_file = file.is_file();
    let dir_handle = unsafe {
        CreateFileW(
            file.parent()
                .unwrap_or_else(|| Path::new("."))
                .as_os_str()
                .encode_wide()
                .chain(Some(0)),
            FILE_LIST_DIRECTORY,
            FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
            std::ptr::null_mut(),
            OPEN_EXISTING,
            FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
            std::ptr::null_mut(),
        )
    };

    if dir_handle == INVALID_HANDLE_VALUE {
        return Err(io::Error::last_os_error());
    }

    let overlapped = OVERLAPPED {
        ..OVERLAPPED {
                Internal:$crate::default::Default::default(),InternalHigh:$crate::default::Default::default(),u:$crate::default::Default::default(),hEvent:$crate::default::Default::default(),
            }
    };
    let event_handle =
        unsafe { CreateEventW(std::ptr::null_mut(), false, false, std::ptr::null()) };

    if event_handle.is_null() {
        unsafe {
            CloseHandle(dir_handle);
        }
        return Err(io::Error::last_os_error());
    }

    overlapped.hEvent = event_handle;

    let mut bytes_transferred = 0;

    let result = unsafe {
        ReadDirectoryChangesW(
            dir_handle,
            as_mut_c_void(buffer),
            buffer.len() as DWORD,
            true as i32,
            FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE,
            std::ptr::null_mut(),
            &mut overlapped,
            None,
        )
    };

    if result == 0 {
        unsafe {
            CloseHandle(dir_handle);
            CloseHandle(event_handle);
        }
        return Err(io::Error::last_os_error());
    }

    let wait_result = unsafe { winapi::um::synchapi::WaitForSingleObject(event_handle, 500) };

    if wait_result != WAIT_OBJECT_0 && wait_result != WAIT_TIMEOUT {
        unsafe {
            CloseHandle(dir_handle);
            CloseHandle(event_handle);
        }
        return Err(io::Error::last_os_error());
    }

    unsafe {
        GetOverlappedResult(
            dir_handle,
            &mut overlapped,
            &mut bytes_transferred,
            false as i32,
        )
    };

    let mut offset = 0;
    let mut notify_info = buffer.as_ptr() as *const FILE_NOTIFY_INFORMATION;

    while !notify_info.is_null() {
        let action = (unsafe { *notify_info }).Action;
        let file_name_len = (unsafe { *notify_info }).FileNameLength / 2;
        let file_name = {
            let slice = unsafe {
                std::slice::from_raw_parts((*notify_info).FileName.as_ptr(), file_name_len as usize)
            };
            String::from_utf16_lossy(slice)
        };

        if is_file
            && file.exists()
            && file.metadata().map_or(false, |m| m.file_size() > 1000)
            && action == FILE_ACTION_MODIFIED
            && *file.file_name().unwrap_or_default() == *file_name
        {
            if let Some(callback) = &mut callback {
                callback(file.to_path_buf());
            }
        }

        if (unsafe { *notify_info }).NextEntryOffset == 0 {
            break;
        }

        offset += (unsafe { *notify_info }).NextEntryOffset;
        notify_info =
            (buffer.as_ptr() as usize + offset as usize) as *const FILE_NOTIFY_INFORMATION;
    }

    unsafe {
        CloseHandle(dir_handle);
        CloseHandle(event_handle);
    }

    Ok(())
}

pub fn watch_file_changes<P, C, E>(
    path: P,
    callback: C,
    error_callback: E,
) -> io::Result<FileWatcher>
where
    P: AsRef<Path>,
    C: FnMut(PathBuf) + Send + 'static,
    E: FnMut(&str) + Send + 'static,
{
    FileWatcher::new(path, Box::new(callback), Box::new(error_callback))
}
